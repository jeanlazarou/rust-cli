# Solution

The first Rust program in this liveProject lays the foundation for future milestones by implementing the basic functional requirements. Consequently this example code is a Rust binary project complete with the necessary dependencies for parsing command line arguments (`clap`), fetching finance data (`yahoo_finance_api`), and working with dates (`chrono`). If you run the code from within `cargo`, use the `--` notation to pass arguments directly to your code and your output should look as follows:

~~~
$ cargo run -- --from 2019-07-03T12:00:09Z  --symbols LYFT,MSFT,AAPL,UBER,LYFT,FB,AMD,GOOG
period start,symbol,price,change %,min,max,30d avg
2019-07-03T12:00:09+00:00,LYFT,$30.50,-49.67%,$16.05,$67.45,$30.94
2019-07-03T12:00:09+00:00,MSFT,$213.29,57.07%,$130.60,$216.54,$206.13
2019-07-03T12:00:09+00:00,AAPL,$438.66,117.16%,$191.06,$438.66,$381.86
2019-07-03T12:00:09+00:00,UBER,$32.68,-26.11%,$14.82,$44.53,$31.69
2019-07-03T12:00:09+00:00,LYFT,$30.50,-49.67%,$16.05,$67.45,$30.94
2019-07-03T12:00:09+00:00,FB,$249.83,26.69%,$146.01,$253.67,$238.01
2019-07-03T12:00:09+00:00,AMD,$85.04,172.65%,$27.99,$85.04,$59.97
2019-07-03T12:00:09+00:00,GOOG,$1464.97,30.62%,$1056.62,$1568.49,$1491.47
~~~

For the following milestone we will build on this code. 


use criterion::{black_box, criterion_group, criterion_main, Criterion};
use milestone_3_tools::{max, min, n_window_sma, price_diff};
use async_std::task;
use rand::Rng;



pub fn min_benchmark(c: &mut Criterion) {
  let mut rng = rand::thread_rng();
  let timeseries: Vec<f64> = (0..5_000).map(|_| rng.gen::<f64>()).collect();

    c.bench_function("min", |b| b.iter(|| task::block_on(min(black_box(&timeseries)))));
}

pub fn max_benchmark(c: &mut Criterion) {
  let mut rng = rand::thread_rng();
  let timeseries: Vec<f64> = (0..5_000).map(|_| rng.gen::<f64>()).collect();

    c.bench_function("max", |b| b.iter(|| task::block_on(max(black_box(&timeseries)))));
}

pub fn sma_benchmark(c: &mut Criterion) {
  let mut rng = rand::thread_rng();
  let timeseries: Vec<f64> = (0..3000).map(|_| rng.gen::<f64>()).collect();
  
  c.bench_function("sma 10d 1k", |b| b.iter(|| task::block_on(n_window_sma(10, black_box(&timeseries[0 .. 1000])))));
  c.bench_function("sma 10d 2k", |b| b.iter(|| task::block_on(n_window_sma(10, black_box(&timeseries[0 .. 2000])))));
  c.bench_function("sma 10d 3k", |b| b.iter(|| task::block_on(n_window_sma(10, black_box(&timeseries)))));

  c.bench_function("sma 20d 1k", |b| b.iter(|| task::block_on(n_window_sma(20, black_box(&timeseries[0 .. 1000])))));
  c.bench_function("sma 20d 2k", |b| b.iter(|| task::block_on(n_window_sma(20, black_box(&timeseries[0 .. 2000])))));
  c.bench_function("sma 20d 3k", |b| b.iter(|| task::block_on(n_window_sma(20, black_box(&timeseries)))));
  
  c.bench_function("sma 30d 1k", |b| b.iter(|| task::block_on(n_window_sma(30, black_box(&timeseries[0 .. 1000])))));
  c.bench_function("sma 30d 2k", |b| b.iter(|| task::block_on(n_window_sma(30, black_box(&timeseries[0 .. 2000])))));
  c.bench_function("sma 30d 3k", |b| b.iter(|| task::block_on(n_window_sma(30, black_box(&timeseries)))));
  
}


pub fn price_diff_benchmark(c: &mut Criterion) {
  let mut rng = rand::thread_rng();
  let timeseries: Vec<f64> = (0..3000).map(|_| rng.gen::<f64>()).collect();
  
  c.bench_function("sma 10d 1k", |b| b.iter(|| task::block_on(price_diff(black_box(&timeseries[0 .. 1000])))));
  c.bench_function("sma 10d 2k", |b| b.iter(|| task::block_on(price_diff(black_box(&timeseries[0 .. 2000])))));
  c.bench_function("sma 10d 3k", |b| b.iter(|| task::block_on(price_diff(black_box(&timeseries)))));

  c.bench_function("sma 20d 1k", |b| b.iter(|| task::block_on(price_diff(black_box(&timeseries[0 .. 1000])))));
  c.bench_function("sma 20d 2k", |b| b.iter(|| task::block_on(price_diff(black_box(&timeseries[0 .. 2000])))));
  c.bench_function("sma 20d 3k", |b| b.iter(|| task::block_on(price_diff(black_box(&timeseries)))));
  
  c.bench_function("sma 30d 1k", |b| b.iter(|| task::block_on(price_diff(black_box(&timeseries[0 .. 1000])))));
  c.bench_function("sma 30d 2k", |b| b.iter(|| task::block_on(price_diff(black_box(&timeseries[0 .. 2000])))));
  c.bench_function("sma 30d 3k", |b| b.iter(|| task::block_on(price_diff(black_box(&timeseries)))));
  
}



criterion_group!(benches, sma_benchmark, min_benchmark, max_benchmark);
criterion_main!(benches);
# Solution

The solution's output didn't change from the previous milestone, so the output still looks as follows:

~~~
$ cargo run -- --from 2019-07-03T12:00:09Z  --symbols LYFT,MSFT,AAPL,UBER,LYFT,FB,AMD,GOOG
period start,symbol,price,change %,min,max,30d avg
2020-08-04T19:30:00+00:00,LYFT,$30.51,-49.98%,$14.81,$67.93,$29.45
2020-08-04T19:30:00+00:00,MSFT,$213.35,55.60%,$131.65,$216.56,$207.69
2020-08-04T19:30:00+00:00,AAPL,$438.66,115.73%,$192.88,$441.84,$415.60
2020-08-04T19:30:00+00:00,UBER,$32.68,-25.85%,$14.39,$44.73,$30.94
2020-08-04T19:30:00+00:00,LYFT,$30.51,-49.98%,$14.81,$67.93,$29.45
2020-08-04T19:30:00+00:00,FB,$249.81,26.98%,$137.91,$254.56,$246.29
2020-08-04T19:30:00+00:00,AMD,$85.05,174.98%,$27.72,$85.05,$79.13
2020-08-04T19:30:00+00:00,GOOG,$1465.19,30.39%,$1016.97,$1576.59,$1495.14
2020-08-04T19:30:00+00:00,LYFT,$30.51,-49.98%,$14.81,$67.93,$29.45
2020-08-04T19:30:00+00:00,MSFT,$213.35,55.60%,$131.65,$216.56,$207.69
2020-08-04T19:30:00+00:00,AAPL,$438.66,115.73%,$192.88,$441.84,$401.40
2020-08-04T19:30:00+00:00,UBER,$32.68,-25.85%,$14.39,$44.73,$30.94
2020-08-04T19:30:00+00:00,LYFT,$30.51,-49.98%,$14.81,$67.93,$29.45
2020-08-04T19:30:00+00:00,FB,$249.81,26.98%,$137.91,$254.56,$246.29
^C
~~~

The solution splits the project along "semantic" lines (i.e. executable and utilities) to loosely couple the projects together. Especially since the utilities project has few dependencies, it can easily cross compile to other operating systems with minimal changes to the project. `Criterion.rs` provides the benchmarking framework to improve implementation performance. This is the solution's benchmark output:

~~~
$ cargo bench
 [...]
     Running target/release/deps/tools_bench-c3b80cf1c1e6ecb4
Gnuplot not found, using plotters backend
sma 10d 1k              time:   [11.584 us 12.316 us 13.196 us]
Found 10 outliers among 100 measurements (10.00%)
  6 (6.00%) high mild
  4 (4.00%) high severe

sma 10d 2k              time:   [15.104 us 16.049 us 17.190 us]
Found 5 outliers among 100 measurements (5.00%)
  2 (2.00%) high mild
  3 (3.00%) high severe

sma 10d 3k              time:   [19.201 us 19.487 us 19.839 us]
Found 11 outliers among 100 measurements (11.00%)
  6 (6.00%) high mild
  5 (5.00%) high severe

sma 20d 1k              time:   [14.064 us 15.262 us 16.714 us]
Found 8 outliers among 100 measurements (8.00%)
  3 (3.00%) high mild
  5 (5.00%) high severe

sma 20d 2k              time:   [21.995 us 24.631 us 27.979 us]
Found 9 outliers among 100 measurements (9.00%)
  2 (2.00%) high mild
  7 (7.00%) high severe

sma 20d 3k              time:   [27.891 us 28.388 us 28.907 us]
Found 5 outliers among 100 measurements (5.00%)
  4 (4.00%) high mild
  1 (1.00%) high severe

sma 30d 1k              time:   [18.010 us 18.574 us 19.287 us]
Found 5 outliers among 100 measurements (5.00%)
  3 (3.00%) high mild
  2 (2.00%) high severe

sma 30d 2k              time:   [30.834 us 32.635 us 34.628 us]
Found 14 outliers among 100 measurements (14.00%)
  6 (6.00%) high mild
  8 (8.00%) high severe

sma 30d 3k              time:   [43.027 us 44.329 us 45.812 us]
Found 7 outliers among 100 measurements (7.00%)
  5 (5.00%) high mild
  2 (2.00%) high severe

min                     time:   [15.652 us 15.807 us 15.967 us]
Found 5 outliers among 100 measurements (5.00%)
  4 (4.00%) high mild
  1 (1.00%) high severe

max                     time:   [15.202 us 15.273 us 15.352 us]
Found 13 outliers among 100 measurements (13.00%)
  8 (8.00%) high mild
  5 (5.00%) high severe
~~~
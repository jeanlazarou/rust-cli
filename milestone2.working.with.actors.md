## Working with actors

**Objective**

For the second milestone you want to build on the features of milestone 1 and increase the scalability of the prototype - the CTO wants to see the S&P 500 index by the end of this milestone. In order to make sure that there are no accidental changes, you want to add tests as well. On top of that, the program should now continously output the CSV data from the previous milestone to capture price changes as soon as possible. You decide to introduce asynchronous processing methods (actors) to create an impressively scalable data processing application. 

**Workflow**

1. Write tests to make sure evolving the code won't break it:
   * Your min and max functions
   * The simple moving average 
   * For calculating the relative and absolute differences over the entire period

1. Continuously fetch stock quotes for each symbol: 
   * It's critical to fetch the data for *every* 30 second interval
   * No explicit thread-related code is required
   * Use actors to run the previous data processing algorithms (min, max, 30 day simple moving average, price change)

1. Transform your current code to be asynchronous:
   * Testing async functions works just as if they were running in the main function.
   * Mixing async libraries may lead to strange errors and incompatibilities. Ideally you can stick to something like [async-std](https://docs.rs/async-std)'s [smol](https://github.com/stjepang/smol) or [tokio](https://tokio.rs).
   * [yahoo_finance_api](https://crates.io/crates/yahoo_finance_api)'s default API is asnyc starting with version 0.3. Remove the `blocking` feature from your `Cargo.toml` to use it.

1. Use actors to do the actual data processing:
   * Wrap your code to work in actors
   * Find ways to publish and subscribe to messages without explicit calls
   * What happens if an actor panics?
   * There are several actor frameworks available. Choose wisely

1. The CTO liked the previous console output format, so you decide to keep it: 
   ~~~csv
   period start,symbol,price,change %,min,max,30d avg
   2020-07-02T19:30:00+00:00,MSFT,$206.25,50.42%,$131.65,$207.85,$202.35
   2020-07-02T19:30:00+00:00,AAPL,$364.12,79.07%,$192.88,$371.38,$363.40
   2020-07-02T19:30:00+00:00,UBER,$30.68,-30.39%,$14.39,$44.73,$30.38
   ~~~

1. Polish your code
   * Can you write more tests to document function limitations and usage?
   * Are you missing data points because of an increasing backlog?
   * Are there other ways to implement this data processing pipeline?

1. Run test with many symbols, like those contained in the [S&P 500](https://www.marketwatch.com/investing/index/spx) index.
   * You can download a list of the S&P 500 May 2020 symbols [here](sp500.txt)

**Importance to project**

Rust's async journey is brief but characterized by a long planning process with many ideas how to realize syntax and implementation. Knowing how to work with the components involved (libraries, I/O runtimes, testing, syntax) will make your software more performant, efficient, and correct - especially at the systems level. 

The pattern of processing a data stream can be observed in many modern programming languages. Instead of hand-crafting threads or threadpools, the use of a built-in I/O runtime has become the preferred method and actors provide an even higher abstraction, which allows for building code around the a stream of data. The actor system can therefore maximize efficiency and effectiveness while the programmer can focus on the business logic. Today, this pattern is found wherever stateless big data streams are processed since it allows for worry-free scaling, like: 

- Data pipelines
- File-system and network I/O
- Batch-processing

With the processing part finished, the next milestone can focus on performance.

**Notes**

* This milestone may create a lot more code than the previous one. Use modules where appropriate.
* Quotes are refreshed every minute, if the US markets are open (i.e. not on weekends/public holidays and outside of banking hours). However, while this data source is free and doesn't require signing up, use a different source if you like.
* Use more sophisticated financial performance indicators if you want.  
* Note that the operating system may use a buffer for printing to stdout.
* `cargo` features are especially important for providing sync and async versions of a crate or adding [compatibility layers](https://docs.rs/async-std/#features). 

**Resources**


* [Chapter 1](https://livebook.manning.com/book/reactive-application-development/chapter-1/) from the [Reactive Application Development book](https://livebook.manning.com/book/reactive-application-development/) by Brian Hanafee, Duncan DeVore, and Sean Walsh provides a great introduction to reactive programming concepts with the popular Akka (Java)

* [Chapter 8.1](https://livebook.manning.com/book/akka-in-action/chapter-8/1) from the [Akka in Action book](https://livebook.manning.com/book/akka-in-action/) book by Raymond Roestenburg, Rob Bakker, and Rob Williams talks about structural patterns (using Akka and Scala) in reactive, actor-based programming.

* [The Rust book](https://doc.rust-lang.org/stable/book/) features a [chapter](https://doc.rust-lang.org/stable/book/ch11-00-testing.html) on writing (non-async) tests.

* [The Rust Async book](https://book.async.rs) provides a general introduction to the Rust and async topics, especially [Futures in chapter 2.1](https://book.async.rs/concepts/futures.html).

* [Xactor](https://docs.rs/xactor/) is an easy-to-understand actor framework built on async-std. If actors are new for you, this is a good place to start.

* [Actix](https://actix.rs/) is a popular and mature actor framework which also powers actix-web, a battle-tested HTTP server. Check out [their book](https://actix.rs/book/actix/sec-0-quick-start.html) to learn about the actor system. 

* [Rust by example](https://doc.rust-lang.org/rust-by-example) contains many snippets for a wide variety of topics, such as testing in [chapter 21](https://doc.rust-lang.org/stable/rust-by-example/testing.html).

* [async-std](https://docs.rs/async-std/) by the Rust project aims to be the standard library for anything async in Rust. Among other things, check out their [chapter on testing](https://docs.rs/async-std/1.6.2/async_std/attr.test.html).

* [The yahoo_finance_api docs](https://docs.rs/yahoo_finance_api/) feature examples of using the crate.

* [cargo](https://doc.rust-lang.org/cargo/) has a way to specify a [crate's features separately](https://doc.rust-lang.org/cargo/reference/features.html).